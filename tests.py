import unittest
import sat
from classes.tripleValueLogic import TripleBoolean


class SATsolverTests(unittest.TestCase):
    def test_get_single_literal(self):
        self.assertEqual(sat.get_single_literal(sat.get_formula("cnf.txt"),
                                                {"2": TripleBoolean(TripleBoolean.false_type)},
                                                {}),
                         sat.Literal("1", False))
        self.assertEqual(sat.get_single_literal(sat.get_formula("cnf2.txt"),
                                                {"1": TripleBoolean(TripleBoolean.true_type)},
                                                {}
                                                ),
                         None)

    def test_compute_clause_value(self):
        self.assertEqual(sat.compute_clause_value((sat.get_formula("cnf2.txt")[1]),
                                                  {"1": TripleBoolean(TripleBoolean.false_type),
                                                   "3": TripleBoolean(TripleBoolean.false_type)},
                                                  {"2": TripleBoolean(TripleBoolean.false_type)}
                                                  ),
                         TripleBoolean(TripleBoolean.false_type))

if __name__ == '__main__':
    unittest.main()
