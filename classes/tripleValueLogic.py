class TripleBoolean:
    true_type = "true"
    false_type = "false"
    maybe_type = "maybe"

    _values = {true_type: 2, maybe_type: 1, false_type: 0}
    _reverse_values = {2: true_type, 1: maybe_type, 0: false_type}
    _not_values = {true_type: false_type, false_type: true_type, maybe_type: maybe_type}

    def __init__(self, type):
        self._type = type

    def __str__(self):
        return self.get_type()

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __repr__(self):
        return self.get_type()

    def get_type(self):
        return self._type

    def conjunction(self, compared):
        return self._process(compared, min)

    def alternative(self, compared):
        return self._process(compared, max)

    def negation(self):
        new_value = TripleBoolean._not_values[self._type]
        return TripleBoolean(new_value)

    def _process(self, compared, fun):
        self_value = TripleBoolean._values[self._type]
        compared_value = TripleBoolean._values[compared.get_type()]
        new_value = fun(self_value, compared_value)
        new_type = TripleBoolean._reverse_values[new_value]
        return TripleBoolean(new_type)