from classes.tripleValueLogic import TripleBoolean
import random
import re


def get_formula(file):
    f = open(file, "r")
    formula_to_parse = []
    for line in f:
        line_list = line.split()
        if line_list[-1] == "0":
            del line_list[-1]
        formula_to_parse.append(line_list)
    lf = LiteralsFactory()
    lf.get_instances(formula_to_parse)
    final_formula = []
    for clause in formula_to_parse:
        final_clause = []
        for literal in clause:
            final_clause.append(lf.instances[literal])
        final_formula.append(final_clause)
    return final_formula


class LiteralsFactory:
    def __init__(self):
        self.instances = {}
        self._negated_regex = re.compile("-")

    def get_instances(self, formula):
        for clause in formula:
            for element in clause:
                x = Literal(self._negated_regex.sub("", element), element.startswith("-"))
                if x not in list(self.instances.values()):
                    self.instances[element] = x
        return self.instances


class Literal:
    def __init__(self, name, is_negated):
        self.name = name
        self.is_negated = is_negated

    def __repr__(self):
        return str((self.name, self.is_negated))

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __hash__(self):
        return hash((self.name, self.is_negated))


def get_variables(formula):
    variables = set()
    for clause in formula:
        for literal in clause:
            variables.add(literal.name)
    return variables


def compute_formula_value(formula, guessed, inferred):
    current_value = TripleBoolean(TripleBoolean.true_type)
    for clause in formula:
        clause_value = compute_clause_value(clause, guessed, inferred)
        current_value = current_value.conjunction(clause_value)
        if current_value.get_type() == TripleBoolean.false_type:
            break
    return current_value


def compute_clause_value(clause, guessed, inferred):
    current_value = TripleBoolean(TripleBoolean.false_type)
    for literal in clause:
        literal_value = TripleBoolean(TripleBoolean.maybe_type)
        if literal.name in guessed:
            literal_value = guessed[literal.name]
        elif literal.name in inferred:
            literal_value = inferred[literal.name]
        if literal.is_negated:
            literal_value = literal_value.negation()
        current_value = current_value.alternative(literal_value)
        if current_value.get_type() == TripleBoolean.true_type:
            break
    return current_value


def choose_variable(dictionary1, dictionary2, variables):
    dictionary1.update(dictionary2)
    dict_keys = list(dictionary1.keys())
    left_vars = []
    for element in variables:
        if element not in dict_keys:
            left_vars.append(element)
    return random.choice(left_vars)


def add_clause(dictionary):
    clause = []
    for item in dictionary:
        clause.append(Literal(item, dictionary[item].get_type() == TripleBoolean.true_type))
    return clause


branches = [(TripleBoolean(TripleBoolean.true_type), TripleBoolean(TripleBoolean.false_type)),
            (TripleBoolean(TripleBoolean.false_type), TripleBoolean(TripleBoolean.true_type))]


def get_random_branching():
    return random.choice(branches)


def solve(formula):
    def keep(state, clause):
        guessed, inferred = state
        value = compute_clause_value(clause, guessed, inferred)
        if value.get_type() == TripleBoolean.false_type:
            return False
        else:
            return True

    states = [({}, {})]
    variables = get_variables(formula)
    while len(states) > 0:
        guessed, inferred = states.pop()
        variable = choose_variable(guessed, inferred, variables)
        left, right = get_random_branching()
        left_state = guessed.copy()
        left_state[variable] = left
        """ Wywołanie metody "provoke_conflicts" na lewym odgałęzieniu"""

        result, new_clause, new_inferred = provoke_conflicts(formula, left_state, inferred)
        if result.get_type() == TripleBoolean.true_type:
            left_state.update(new_inferred)
            final_assignments = left_state
            return True, final_assignments
        elif result.get_type() == TripleBoolean.maybe_type:
            states.append((left_state, new_inferred))
        #elif len(new_clause) > 0:
            #states = list(filter(lambda state: keep(state, new_clause), states))
            #formula.append(new_clause)
        right_state = guessed.copy()
        right_state[variable] = right
        """Wywołanie metody provoke_conflicts na prawym odgałęzieniu"""

        result, new_clause, new_inferred = provoke_conflicts(formula, right_state, inferred)
        if result.get_type() == TripleBoolean.true_type:
            right_state.update(new_inferred)
            final_assignments = right_state
            return True, final_assignments
        elif result.get_type() == TripleBoolean.maybe_type:
            states.append((right_state, new_inferred))
        #elif len(new_clause) > 0:
            #states = list(filter(lambda state: keep(state, new_clause), states))
            #formula.append(new_clause)
    return False, {}


def provoke_conflicts(formula, guessed, inferred):
    formula_value = compute_formula_value(formula, guessed, inferred)
    if formula_value.get_type() == TripleBoolean.true_type:
        return formula_value, [], inferred
    elif formula_value.get_type() == TripleBoolean.false_type:
        new_clause = add_clause(guessed)
        return formula_value, new_clause, inferred
    else:
        formula_value, new_inferred = unit_propagation(formula, guessed, inferred)
        if formula_value.get_type() == TripleBoolean.true_type:
            return formula_value, [], new_inferred
        elif formula_value.get_type() == TripleBoolean.false_type:
            new_clause = add_clause(guessed)
            return formula_value, new_clause, inferred
        else:
            return formula_value, [], new_inferred


def unit_propagation(formula, guessed, inferred):
    new_inferred = inferred.copy()
    changes = True
    while changes:
        changes = False
        literal_to_add = get_single_literal(formula, guessed, new_inferred)
        if literal_to_add is not None:
            if literal_to_add.is_negated:
                new_inferred[literal_to_add.name] = TripleBoolean(TripleBoolean.false_type)
            else:
                new_inferred[literal_to_add.name] = TripleBoolean(TripleBoolean.true_type)
            changes = True
            result_after_change = compute_formula_value(formula, guessed, new_inferred)
            if result_after_change.get_type() == TripleBoolean.true_type:
                return TripleBoolean(TripleBoolean.true_type), new_inferred
            elif result_after_change.get_type() == TripleBoolean.false_type:
                return TripleBoolean(TripleBoolean.false_type), new_inferred
    return TripleBoolean(
        TripleBoolean.maybe_type), new_inferred


def get_single_literal(formula, guessed, inferred):
    literals_dict = {}
    literals_dict.update(guessed)
    literals_dict.update(inferred)
    for clause in formula:
        clause_value = compute_clause_value(clause, guessed, inferred)
        if clause_value.get_type() == TripleBoolean.maybe_type:
            unassigned_literals = set()
            for literal in clause:
                if literal.name not in literals_dict:
                    unassigned_literals.add(literal)
            if len(unassigned_literals) == 1:
                return unassigned_literals.pop()
    return None


a = solve(get_formula("cnf4.txt"))
print(a)
